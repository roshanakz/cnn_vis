from keras.applications import vgg16, vgg19, ResNet50
from keras import backend as K
from scipy.misc import imsave
import scipy.ndimage as ndimage
import numpy as np
import cv2

model = vgg19.VGG19(weights='imagenet', include_top=False)
layer1 = model.layers[1]
weights1 = layer1.get_weights()
weights1_0 = weights1[0]

dim = 2

img_width = dim*3
img_height = dim*3
n = 8
margin = 5
width = n * img_width + (n - 1) * margin
height = n * img_height + (n - 1) * margin
stitched_filters = np.zeros((width, height, 3))
k = 0
for i in range(n):
    for j in range(n):
        stitched_filters[(img_width + margin) * i: (img_width + margin) * i + img_width,
                         (img_height + margin) * j: (img_height + margin) * j + img_height, :] = ndimage.zoom(np.squeeze(weights1_0[:,:,:,k]), (dim, dim, 1))
	k = k +1

imsave('/home/zakizadeh/projects/CNN_Visualization/vgg19_testall_zoom_layer1.png', stitched_filters)
