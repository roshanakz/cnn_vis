
%load('imagenet-vgg-verydeep-19.mat')
load('imagenet-matconvnet-vgg-m.mat')
weights_conv1 = double(layers{1,1}.weights{1,1});

figure,
for i= 1:96
   filter_img =  weights_conv1(:,:,:,i) - min(weights_conv1(:));
   filter_img = filter_img./max(filter_img(:));
   
   subplot(8,12,i), imshow(imresize(filter_img, 30).^3.9);

end